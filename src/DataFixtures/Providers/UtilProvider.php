<?php declare(strict_types=1);


namespace App\DataFixtures\Providers;

/**
 * Class UtilProvider
 * @package App\DataFixtures\Providers
 */
class UtilProvider
{

    /**
     * VideoTitle
     * @param $index
     * @return string
     */
    public function videoTitle(int $index): string
    {
        return $this->getYoutubeFixtureData($index)['title'];
    }

    /**
     * VideoUrl
     * @param $index
     * @return string
     */
    public function videoUrl(int $index): string
    {
        return $this->getYoutubeFixtureData($index)['url'];
    }

    /**
     * GetYoutubeFixtureData
     * @param  int|null  $index
     * @return array
     */
    private function getYoutubeFixtureData(?int $index = null): array
    {
        $videos = [

            1  => [
                'title' => 'Authentic Greek Food Cooking',
                'url'   => 'https://www.youtube.com/embed/0_s9MNUpRx0',
            ],
            2  => [
                'title' => 'Chili con carne',
                'url'   => 'https://www.youtube.com/embed/pnYn5Z1pOWY',
            ],
            3  => [
                'title' => '4 Ingredient butter cookies',
                'url'   => 'https://www.youtube.com/embed/H7l1NlOnTWo',
            ],
            4  => [
                'title' => 'Classic carbonara',
                'url'   => 'https://www.youtube.com/embed/6vrVyK8f5NA',
            ],
            5  => [
                'title' => 'Homemade cream cheese',
                'url'   => 'https://www.youtube.com/embed/K85oFkLWaXw',
            ],
            6  => [
                'title' => 'How to make whipped cream',
                'url'   => 'https://www.youtube.com/embed/-4ty64nxRzw',
            ],
            7  => [
                'title' => 'Traditional Greek Fava',
                'url'   => 'https://www.youtube.com/embed/1p7DmwMKv18',
            ],
            8  => [
                'title' => 'Mashed Sweet Potatoes',
                'url'   => 'https://www.youtube.com/embed/Et2y4L_lckc',
            ],
            9  => [
                'title' => 'Homemade Croutons',
                'url'   => 'https://www.youtube.com/embed/PpZzxCOHIm0',
            ],
            10 => [
                'title' => 'Caramel Sauce in 3 minutes',
                'url'   => 'https://www.youtube.com/embed/umAR61HnHpU',
            ],
            11 => [
                'title' => 'Cornbread with feta cheese',
                'url'   => 'https://www.youtube.com/embed/0SQiSEDOSjA',
            ],
            12 => [
                'title' => 'Homemade Mayonnaise',
                'url'   => 'https://www.youtube.com/embed/EF7jynA6ey8',
            ],


        ];

        return $index ? $videos[$index] : $videos;
    }

}