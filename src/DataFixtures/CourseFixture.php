<?php declare(strict_types=1);

namespace App\DataFixtures;

use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class CourseFixture
 * @package App\DataFixtures
 */
class CourseFixture extends BaseFixture
{

    protected string $aliceFixtureFile = 'course';

    public function load(ObjectManager $manager)
    {
        $this->loadAliceFixture($manager);
    }
}