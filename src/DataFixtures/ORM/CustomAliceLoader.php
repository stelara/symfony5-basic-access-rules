<?php declare(strict_types=1);


namespace App\DataFixtures\ORM;

use App\DataFixtures\Providers\UtilProvider;
use Faker\Generator;
use Nelmio\Alice\Faker\Provider\AliceProvider;
use Nelmio\Alice\Loader\NativeLoader;
use Faker\Factory as FakerGeneratorFactory;

class CustomAliceLoader extends NativeLoader
{

    /**
     * CreateFakerGenerator
     * @return Generator
     */
    protected function createFakerGenerator(): Generator
    {
        $generator = FakerGeneratorFactory::create(parent::LOCALE);
        $generator->addProvider(new AliceProvider());
        $generator->addProvider(new UtilProvider());
        $generator->seed($this->getSeed());

        return $generator;
    }
}