<?php declare(strict_types=1);

namespace App\DataFixtures;

use Doctrine\Common\Persistence\ObjectManager;
use App\DataFixtures\ORM\CustomAliceLoader as NativeLoader;
use Doctrine\Bundle\FixturesBundle\Fixture;

/**
 * Class BaseFixture
 * @package App\DataFixtures
 */
abstract class BaseFixture extends Fixture
{

    /**
     * @var string $aliceFixtureFile
     */
    protected string $aliceFixtureFile = 'main.yaml';

    /**
     * const FIXTURE_DATA_DIR
     */
    protected const FIXTURE_DATA_DIR = 'alice_data';

    /**
     * LoadAliceFixture
     * @param  ObjectManager  $manager
     */
    protected function loadAliceFixture(ObjectManager $manager): void
    {
        $this->aliceFixtureFile = pathinfo(
                $this->aliceFixtureFile,
                PATHINFO_FILENAME
            ).'.yaml';

        $objects = (new NativeLoader())->loadFile(
            $this->getAliceFixturesPath().$this->aliceFixtureFile
        )->getObjects();

        if (count($objects)) {
            foreach ($objects as $entity) {
                $manager->persist($entity);
            }
            $manager->flush();
        }
    }

    /**
     * GetAliceFixturesPath
     * @return string
     */
    protected function getAliceFixturesPath(): string
    {
        return __DIR__.DIRECTORY_SEPARATOR.
            static::FIXTURE_DATA_DIR.DIRECTORY_SEPARATOR;
    }
}