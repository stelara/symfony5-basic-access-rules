<?php declare(strict_types=1);

namespace App\DataFixtures;

use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class UserFixture
 * @package App\DataFixtures
 */
class UserFixture extends BaseFixture
{

    protected string $aliceFixtureFile = 'user';

    public function load(ObjectManager $manager)
    {
        $this->loadAliceFixture($manager);
    }
}
