<?php declare(strict_types=1);


namespace App\Doctrine\Traits;


use Doctrine\ORM\Mapping as ORM;

/**
 * Trait Timestamps
 */
trait Status
{

    /**
     * @ORM\Column(type="boolean")
     */
    private $isActive;

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }
}