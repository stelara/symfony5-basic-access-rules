<?php declare(strict_types=1);


namespace App\Events;


use App\Entity\Course;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class CoursePreview
 * @package App\Events
 */
class CoursePreview
{

    public const NAME = 'course.preview';

    /**
     * @var UserInterface $user
     */
    private UserInterface $user;
    /**
     * @var Course
     */
    private Course $course;


    /**
     * CoursePreview constructor.
     * @param  Course  $course
     * @param  UserInterface  $user
     */
    public function __construct(Course $course, UserInterface $user)
    {
        $this->user   = $user;
        $this->course = $course;
    }

    /**
     * GetUser
     * @return UserInterface
     */
    public function getUser(): UserInterface
    {
        return $this->user;
    }

    /**
     * GetCourse
     * @return Course
     */
    public function getCourse(): Course
    {
        return $this->course;
    }
}