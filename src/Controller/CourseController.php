<?php declare(strict_types=1);

namespace App\Controller;

use App\CourseAccess\CourseAccess;
use App\Events\CoursePreview;
use App\Managers\CourseManager;
use Psr\EventDispatcher\EventDispatcherInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class CourseController
 * @package App\Controller
 */
class CourseController extends AbstractController
{

    /**
     * @var CourseManager
     */
    private CourseManager $courseManager;

    /**
     * CourseController constructor.
     * @param  CourseManager  $courseManager
     */
    public function __construct(CourseManager $courseManager)
    {
        $this->courseManager = $courseManager;
    }


    /**
     * @Route("/course/{id}", name="video_preview", methods={"GET"}, requirements={"id":"\d+"})
     * @param  int  $id
     * @param  EventDispatcherInterface  $eventDispatcher
     * @param  CourseAccess  $courseAccess
     * @return Response
     */
    public function preview(
        int $id,
        EventDispatcherInterface $eventDispatcher,
        CourseAccess $courseAccess
    ): Response {
        //$this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $course = $this->courseManager->getCourseById($id);

        if (!$course) {
            throw $this->createNotFoundException('Course not found!');
        }

        $showVideoDecision = $courseAccess->makeDecision();

        $showVideoDecision && $eventDispatcher->dispatch(
            new CoursePreview($course, $this->getUser()),
            CoursePreview::NAME
        );

        return $this->render(
            'course/preview.html.twig',
            [
                'course'          => $course,
                'displayVideo'    => $showVideoDecision
            ]
        );
    }
}
