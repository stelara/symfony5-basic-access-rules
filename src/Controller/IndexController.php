<?php declare(strict_types=1);

namespace App\Controller;

use App\Managers\CourseManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class IndexController
 * @package App\Controller
 * @Route("/")
 */
class IndexController extends AbstractController
{

    /**
     * @Route("", name="index")
     * @param  CourseManager  $courseManager
     * @return Response
     */
    public function index(CourseManager $courseManager): Response
    {
        return $this->render(
            'index/index.html.twig',
            [
                'availableCourses' => $courseManager->getActiveVideosHomePage(),
            ]
        );
    }
}
