<?php declare(strict_types=1);

namespace App\Subscribers;

use App\Events\CoursePreview;
use App\Repository\CourseViewRepository;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class CoursePreviewSubscriber implements EventSubscriberInterface
{

    /**
     * @var CourseViewRepository
     */
    private CourseViewRepository $courseViewRepository;

    /**
     * VideoPreviewSubscriber constructor.
     * @param  CourseViewRepository  $courseViewRepository
     */
    public function __construct(
        CourseViewRepository $courseViewRepository
    ) {
        $this->courseViewRepository = $courseViewRepository;
    }

    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents()
    {
        return [
            CoursePreview::NAME => [

                ['updateCourseViews', 10],

            ],
        ];
    }

    /**
     * UpdateCourseViews
     * @param  CoursePreview  $event
     * @throws \Exception
     */
    public function updateCourseViews(CoursePreview $event): void
    {
        $this->courseViewRepository->updateVideoPreview($event->getUser());
    }
}