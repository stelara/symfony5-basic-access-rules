<?php declare(strict_types=1);


namespace App\Utill;

use DateTimeImmutable;

/**
 * Class DateTimeHelper
 * @package App\Utill
 */
class DateTimeHelper
{

    public function getDateIntervalMinutes(DateTimeImmutable $dateInPast)
    {
        $interval = $dateInPast->diff($this->getCurrentDate());

        return ($interval->days * 24 * 60) + ($interval->h * 60) + $interval->i;
    }

    public function getCurrentDate(): DateTimeImmutable
    {
        return new DateTimeImmutable();
    }

}