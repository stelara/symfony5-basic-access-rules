<?php declare(strict_types=1);

namespace App\Managers;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepositoryInterface;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class BaseManager
 * @package App\Managers
 */
abstract class BaseManager
{

    protected const BASE_ENTITY_NAMESPACE = "\\App\\Entity\\";

    /**
     * @var ServiceEntityRepositoryInterface
     */
    private ServiceEntityRepositoryInterface $repository;
    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $entityManager;

    /**
     * @var string $entityClass
     */
    protected string $entityClass;

    /**
     * ProductManager constructor
     * @param  EntityManagerInterface  $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->entityClass   = $this->findEntityClass();

        $this->repository = $this->entityManager->getRepository(
            $this->entityClass
        );
    }

    public function getEntityManager(): EntityManagerInterface
    {
        return $this->entityManager;
    }

    public function getRepository()
    {
        return $this->repository;
    }

    private function findEntityClass()
    {
        if (is_null($this->entityClass)) {
            $this->entityClass = static::BASE_ENTITY_NAMESPACE.str_replace(
                    'Manager',
                    '',
                    get_class($this)
                );
        }

        return $this->entityClass;
    }

    /**
     * @param  string  $alias
     * @param  bool  $status
     * @return Criteria
     */
    public function statusCriteria(string $alias, bool $status = true): Criteria
    {
        return Criteria::create()
            ->andWhere(Criteria::expr()->eq($alias.'.isActive', $status));
    }

}