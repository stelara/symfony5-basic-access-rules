<?php declare(strict_types=1);


namespace App\Managers;

use App\Entity\User;
use \Symfony\Component\Form\Form;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserManager extends BaseManager implements ManagerInterface
{

    /**
     * @var string $entityClass
     */
    protected string $entityClass = 'App\Entity\User';

    /**
     * @var UserPasswordEncoderInterface
     */
    private UserPasswordEncoderInterface $passwordEncoder;

    /**
     * SetUserPasswordEncoder
     * @required
     * @param  UserPasswordEncoderInterface  $passwordEncoder
     */
    public function setUserPasswordEncoder(
        UserPasswordEncoderInterface $passwordEncoder
    ): void {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function registerUser(User $user, Form $form)
    {
        // encode the plain password
        $user->setPassword(
            $this->passwordEncoder->encodePassword(
                $user,
                $form->get('plainPassword')->getData()
            )
        );
        $user->setCreatedAt(New \DateTimeImmutable());
        $user->setRoles(['ROLE_USER']);
        $user->setIsActive(true);

        $em = $this->getEntityManager();
        $em->persist($user);
        $em->flush();
    }
}