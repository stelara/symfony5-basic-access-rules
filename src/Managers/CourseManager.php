<?php declare(strict_types=1);

namespace App\Managers;

class CourseManager extends BaseManager implements ManagerInterface
{

    /**
     * @var string $entityClass
     */
    protected string $entityClass = 'App\Entity\Course';

    /**
     * GetActiveVideosHomePage
     * @return object[]
     */
    public function getActiveVideosHomePage()
    {
        return $this->getRepository()->findBy(['isActive' => true]);
    }

    /**
     * GetCourseById
     * @param  int  $id
     * @return object|null
     */
    public function getCourseById(int $id)
    {
        return $this->getRepository()->findOneBy(
            ['id' => $id, 'isActive' => true]
        );
    }
}