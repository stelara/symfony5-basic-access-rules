<?php

namespace App\Repository;

use App\Entity\CourseView;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @method CourseView|null find($id, $lockMode = null, $lockVersion = null)
 * @method CourseView|null findOneBy(array $criteria, array $orderBy = null)
 * @method CourseView[]    findAll()
 * @method CourseView[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CourseViewRepository extends ServiceEntityRepository
{

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CourseView::class);
    }

    /**
     * UpdateVideoPreview
     * @param  UserInterface  $user
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function updateVideoPreview(UserInterface $user): void
    {
        $courseView = new CourseView();
        $courseView->setUser($user);
        $courseView->setAccessDateTime(new \DateTimeImmutable());
        $em = $this->getEntityManager();
        $em->persist($courseView);
        $em->flush();
    }

    /**
     * GetCourseViewsByUser
     * @param  UserInterface  $user
     * @return int
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getCourseViewsByUser(UserInterface $user): int
    {
        $qb = $this->createQueryBuilder('cview');
        $qb->select([$qb->expr()->count('cview.id')])
            ->andWhere('cview.user = :user')
            ->setParameter('user', $user);

        return $qb->getQuery()->getSingleScalarResult();
    }

    /**
     * GetLastCourseVisitByUser
     * @param  UserInterface  $user
     * @return mixed
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getLastCourseVisitByUser(UserInterface $user)
    {
        $qb = $this->createQueryBuilder('cview');
        $qb->select(['cview.accessDateTime'])
            ->andWhere('cview.user = :user')
            ->setParameter('user', $user)
            ->orderBy('cview.id', 'DESC')
            ->setMaxResults(1);


        return $qb->getQuery()->getSingleScalarResult();
    }
}
