<?php declare(strict_types=1);


namespace App\Security;

use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\Exception\InvalidCsrfTokenException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;
use Symfony\Component\Security\Guard\Authenticator\AbstractFormLoginAuthenticator;
use Symfony\Component\Security\Http\Util\TargetPathTrait;

/**
 * Class LoginFormAuthenticator
 * @package App\Security
 */
class LoginFormAuthenticator extends AbstractFormLoginAuthenticator
{

    use TargetPathTrait;

    /**
     * @var UserRepository
     */
    private UserRepository $userRepository;
    /**
     * @var RouterInterface
     */
    private RouterInterface $router;
    /**
     * @var CsrfTokenManagerInterface
     */
    private CsrfTokenManagerInterface $csrfTokenManager;
    /**
     * @var UserPasswordEncoderInterface
     */
    private UserPasswordEncoderInterface $passwordEncoder;


    /**
     * LoginFormAuthenticator constructor.
     * @param  UserRepository  $userRepository
     * @param  RouterInterface  $router
     * @param  CsrfTokenManagerInterface  $csrfTokenManager
     * @param  UserPasswordEncoderInterface  $passwordEncoder
     */
    public function __construct(
        UserRepository $userRepository,
        RouterInterface $router,
        CsrfTokenManagerInterface $csrfTokenManager,
        UserPasswordEncoderInterface $passwordEncoder
    ) {
        $this->userRepository   = $userRepository;
        $this->router           = $router;
        $this->csrfTokenManager = $csrfTokenManager;
        $this->passwordEncoder  = $passwordEncoder;
    }

    public function supports(Request $request)
    {
        return $request->attributes->get(
                '_route'
            ) === 'main_login' && $request->isMethod('POST');
    }

    public function getCredentials(Request $request): array
    {
        //dd($request->request->all());
        $credentials = [
            'email'      => $request->request->get('email'),
            'password'   => $request->request->get('password'),
            'csrf_token' => $request->request->get('_csrf_token'),
        ];

        $request->getSession()->set(
            Security::LAST_USERNAME,
            $credentials['email']
        );

        return $credentials;
    }

    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        //dd($credentials);

        $token = new CsrfToken('authenticate', $credentials['csrf_token']);

        if (!$this->csrfTokenManager->isTokenValid($token)) {
            throw new InvalidCsrfTokenException();
        }

        $user = $this->userRepository->findOneBy(
            ['email' => $credentials['email']]
        );

        if (!$user) {
            throw new CustomUserMessageAuthenticationException(
                'Invalid credentials!'
            );
        }

        return $user;
    }

    public function checkCredentials($credentials, UserInterface $user)
    {
        //dd($user);
        return $this->passwordEncoder->isPasswordValid(
            $user,
            $credentials['password']
        );
    }


    public function onAuthenticationSuccess(
        Request $request,
        TokenInterface $token,
        $providerKey
    ) {
        //dd('Success');

        if ($targetPath = $this->getTargetPath(
            $request->getSession(),
            $providerKey
        )) {
            return new RedirectResponse($targetPath);
        }

        return new RedirectResponse($this->router->generate('index'));
    }

    protected function getLoginUrl()
    {
        // On fail
        return $this->router->generate('main_login');
    }

}
