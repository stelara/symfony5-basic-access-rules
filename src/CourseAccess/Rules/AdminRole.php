<?php declare(strict_types=1);


namespace App\CourseAccess\Rules;

use App\CourseAccess\CourseAccessRuleInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;

class AdminRole implements CourseAccessRuleInterface
{

    /**
     * @var Security
     */
    private Security $security;

    const REQUIRED_ROLE = 'ROLE_ADMIN';


    /**
     * AdminRole constructor.
     * @param  Security  $security
     */
    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    public function decide(): bool
    {
        $user = $this->security->getUser();

        if ($user instanceof UserInterface){
            return in_array(self::REQUIRED_ROLE, $user->getRoles());
        }

        return false;

    }
}