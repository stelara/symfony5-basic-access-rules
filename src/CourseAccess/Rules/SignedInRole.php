<?php declare(strict_types=1);


namespace App\CourseAccess\Rules;

use App\CourseAccess\CourseAccessRuleInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;

class SignedInRole implements CourseAccessRuleInterface
{

    /**
     * @var Security
     */
    private Security $security;


    /**
     * AdminRole constructor.
     * @param  Security  $security
     */
    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    public function decide(): bool
    {
        $user = $this->security->getUser();

        if ($user instanceof UserInterface){
            return true;
        }

        return false;

    }
}