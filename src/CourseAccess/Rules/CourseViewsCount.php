<?php declare(strict_types=1);


namespace App\CourseAccess\Rules;

use App\CourseAccess\CourseAccessRuleInterface;
use App\Repository\CourseViewRepository;
use Symfony\Component\Security\Core\Security;

/**
 * Class CourseViewsCount
 * @package App\CourseAccess\Rules
 */
class CourseViewsCount implements CourseAccessRuleInterface
{

    const VIEW_COUNT_LIMIT = 10;
    /**
     * @var CourseViewRepository
     */
    private CourseViewRepository $courseViewRepository;
    /**
     * @var Security
     */
    private Security $security;

    /**
     * CourseViewsCount constructor.
     * @param  CourseViewRepository  $courseViewRepository
     * @param  Security  $security
     */
    public function __construct(
        CourseViewRepository $courseViewRepository,
        Security $security
    ) {
        $this->courseViewRepository = $courseViewRepository;
        $this->security             = $security;
    }

    public function decide(): bool
    {
        $courseViews = $this->courseViewRepository->getCourseViewsByUser(
            $this->security->getUser()
        );

        /**
         * OR have viewed less than 10 courses (different or not)
         */
        return intval($courseViews) < (self::VIEW_COUNT_LIMIT - 1);
    }
}