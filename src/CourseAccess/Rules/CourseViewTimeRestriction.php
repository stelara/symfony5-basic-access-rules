<?php declare(strict_types=1);

namespace App\CourseAccess\Rules;

use App\CourseAccess\CourseAccessRuleInterface;
use App\Repository\CourseViewRepository;
use App\Utill\DateTimeHelper;
use DateTimeImmutable;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Security\Core\Security;

/**
 * Class CourseViewTimeRestriction
 * @package App\CourseAccess\Rules
 */
class CourseViewTimeRestriction implements CourseAccessRuleInterface
{

    /**
     * @var CourseViewRepository
     */
    private CourseViewRepository $courseViewRepository;
    /**
     * @var Security
     */
    private Security $security;
    /**
     * @var ParameterBagInterface
     */
    private ParameterBagInterface $parameterBag;

    /**
     * @var int $remainingMinutes
     */
    private int $remainingMinutes = 0;
    /**
     * @var DateTimeHelper
     */
    private DateTimeHelper $dateTimeHelper;

    /**
     * CourseViewsCount constructor.
     * @param  CourseViewRepository  $courseViewRepository
     * @param  Security  $security
     * @param  ParameterBagInterface  $parameterBag
     * @param  DateTimeHelper  $dateTimeHelper
     */
    public function __construct(
        CourseViewRepository $courseViewRepository,
        Security $security,
        ParameterBagInterface $parameterBag,
        DateTimeHelper $dateTimeHelper
    ) {
        $this->courseViewRepository = $courseViewRepository;
        $this->security             = $security;
        $this->parameterBag         = $parameterBag;
        $this->dateTimeHelper       = $dateTimeHelper;
    }

    /**
     * Decide
     * @return bool
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function decide(): bool
    {
        $lastAccess = $this->courseViewRepository->getLastCourseVisitByUser(
            $this->security->getUser()
        );

        $viewTimeRestrictionInMinutes = (int)$this->parameterBag->get(
            'video.view_time_restriction'
        );

        $timeLimit = intval($viewTimeRestrictionInMinutes);

        $minutesDiff = $this->dateTimeHelper->getDateIntervalMinutes(
            new DateTimeImmutable($lastAccess),
        );

        $this->setRemainingMinutes($minutesDiff - $timeLimit);

        return $minutesDiff > $timeLimit;
    }

    /**
     * GetDateObject
     * @param  string|null  $date
     * @return DateTimeImmutable
     * @throws \Exception
     */
    public function getDateObject(?string $date = null): DateTimeImmutable
    {
        return $date ? new DateTimeImmutable($date) : new DateTimeImmutable();
    }

    /**
     * GetRemainingMinutes
     * @return int
     */
    public function getRemainingMinutes(): int
    {
        return $this->remainingMinutes;
    }

    /**
     * SetRemainingMinutes
     * @param  int  $remainingMinutes
     */
    public function setRemainingMinutes(int $remainingMinutes): void
    {
        $this->remainingMinutes = abs($remainingMinutes);
    }
}