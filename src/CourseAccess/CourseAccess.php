<?php declare(strict_types=1);

namespace App\CourseAccess;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class CourseAccess
 * @package App\CourseAccess
 */
class CourseAccess
{

    /**
     * @var ContainerInterface
     */
    private ContainerInterface $container;

    /**
     * @var int $remainingMinutes
     */
    private int $remainingMinutes;

    /**
     * CourseAccess constructor.
     * @param  ContainerInterface  $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * MakeDecision
     * @return bool
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function makeDecision(): bool
    {

        return $this->container->get(
            'video.access.rule.signedin'
        )->decide() && (
            $this->container->get('video.access.rule.admin')->decide() ||
            $this->container->get('video.access.rule.view_count')->decide() ||
            $this->container->get('video.access.rule.time')->decide()

        );
    }

    /**
     * GetDisabledUserMessage
     * @return string
     */
    /*public function getDisabledUserMessage(): string
    {
        $minutesString = $this->remainingMinutes == 1 ? 'minute' : "minutes";

        return ($this->remainingMinutes == 0) ? 'Please wait less than minute!' :
            sprintf(
                "Please wait %d %s!",
                $this->remainingMinutes,
                $minutesString
            );
    }*/
}