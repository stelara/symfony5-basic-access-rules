<?php declare(strict_types=1);


namespace App\CourseAccess;

/**
 * Interface CourseAccessRuleInterface
 * @package App\CourseAccess
 */
interface CourseAccessRuleInterface
{

    public function decide(): bool;
}