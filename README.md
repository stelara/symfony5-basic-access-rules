Basic Symfony 5 setup, db, login, register, specific access rules, unit tests
=======================

## Server/PHP Requirements

* PHP >= 7.4  (PHP used in dev process 7.4.3).
* Web Server used during development - Nginx/php-fpm (VirtualHost).
* Any other Web server should be OK, PHP-Dev server , Apache etc ..
* Database server used during development MySql (5.7.29).
    * Database user should be able to CREATE, DELETE etc... DB's (admin access).
* project/var folder should be writable by WebServer user.

## How to Install


 1.  ```composer install```
 2.  create .env.local (prepare DATABASE_URL - example in .env).
 3.  ```bin/console d:d:c```
 4.  ```bin/console d:s:c```
 5.  ```bin/console d:f:l``` 

Open web-server url in your browser (url depends of Web server configuration). 

## How to run unit tests

* ```bin/phpunit```

## Dev notes

***For the sake of simplicity*** : 

* AutoIncrement fields are in use.
* Only basic security tools and practises are in use.
* No SQL migrations.
* Default error pages are in use. 
* For Admin login please use admin_[1..5]@thcourse.net:admina.
* In order to login with normal user, please register new user.
* PSR-12 compatible.

