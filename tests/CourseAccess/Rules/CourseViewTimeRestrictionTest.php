<?php declare(strict_types=1);
/**
 * (c) Stelio Stefanov <stefanov.stelio@gmail.com>
 */

namespace App\Tests\CourseAccess\Rules;

use App\CourseAccess\Rules\CourseViewTimeRestriction;
use App\Tests\BaseTestCase;
use DateTimeImmutable;
use Generator;

/**
 * Class CourseViewTimeRestrictionTest
 * @package App\Tests\CourseAccess\Rules
 */
class CourseViewTimeRestrictionTest extends BaseTestCase
{

    const TIME_LIMIT_PARAMETER_KEY = 'video.view_time_restriction';

    /**
     * TestDecide Time Restriction
     * @dataProvider dateParamProvider
     * @param  string  $lastDate
     * @param  DateTimeImmutable  $nowDate
     * @param  array  $timeLimit
     * @param  bool  $expectedResult
     */
    public function testDecide(
        string $lastDate,
        DateTimeImmutable $nowDate,
        array $timeLimit,
        bool $expectedResult
    ): void {
        $courseViewsTimeRestriction = new CourseViewTimeRestriction(
            $this->createCourseViewRepositoryDateMock($lastDate),
            $this->createSecurityMock(['ROLE_USER']),
            $this->createParameterBagInterfaceMock(
                $timeLimit,
                self::TIME_LIMIT_PARAMETER_KEY
            ),
            $this->createDateTimeHelperPartialMock($nowDate)
        );

        $this->{$this->getAssertBoolMethod($expectedResult)}(
            $courseViewsTimeRestriction->decide()
        );
    }

    /**
     * DateProvider
     * @return Generator
     */
    public function dateParamProvider()
    {
        yield [
            '2020-01-09 13:24:36',
            new DateTimeImmutable(),
            [self::TIME_LIMIT_PARAMETER_KEY => 40],
            true,
        ];

        yield [
            '2020-02-10 13:24:36',
            new DateTimeImmutable(),
            [self::TIME_LIMIT_PARAMETER_KEY => 1200],
            true,
        ];

        yield [
            '2020-02-10 13:24:36',
            new DateTimeImmutable(),
            [self::TIME_LIMIT_PARAMETER_KEY => 1200],
            true,
        ];

        yield [
            '2020-02-10 13:24:36',
            new DateTimeImmutable('2020-02-10 13:50:36'),
            [self::TIME_LIMIT_PARAMETER_KEY => 120],
            false,
        ];

        yield [
            '2020-03-06 12:24:36',
            new DateTimeImmutable('2020-03-07 11:50:36'),
            [self::TIME_LIMIT_PARAMETER_KEY => 1440],
            false,
        ];
    }
}