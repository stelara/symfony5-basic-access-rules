<?php declare(strict_types=1);


namespace App\Tests\CourseAccess\Rules;

use App\CourseAccess\Rules\AdminRole;
use App\Tests\BaseTestCase;
use Generator;


class AdminRuleTest extends BaseTestCase
{

    /**
     * TestDecide Admin Rule
     * @dataProvider roleProvider
     * @param  array  $role
     * @param  bool  $expectedResult
     */
    public function testDecide(array $role, bool $expectedResult): void
    {
        $adminRule = new AdminRole($this->createSecurityMock($role));

        $this->{$this->getAssertBoolMethod($expectedResult)}(
            $adminRule->decide()
        );
    }

    /**
     * RoleProvider
     * @return Generator
     */
    public function roleProvider()
    {
        yield [['ROLE_ADMIN'], true];
        yield [['ROLE_USER'], false];
        yield [['ROLE_BUSTER'], false];
        yield [[], false];
    }
}