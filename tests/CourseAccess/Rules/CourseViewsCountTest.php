<?php declare(strict_types=1);
/**
 * (c) Stelio Stefanov <stefanov.stelio@gmail.com>
 */

namespace App\Tests\CourseAccess\Rules;

use App\CourseAccess\Rules\CourseViewsCount;
use App\Tests\BaseTestCase;
use Generator;

class CourseViewsCountTest extends BaseTestCase
{

    /**
     * TestDecide Views Count
     * @dataProvider viewsProvider
     * @param  int  $views
     * @param  bool  $expectedResult
     */
    public function testDecide(
        int $views,
        bool $expectedResult
    ): void {
        $courseViewsCount = new CourseViewsCount(
            $this->createCourseViewRepositoryViewsMock($views),
            $this->createSecurityMock(['ROLE_USER'])
        );


        $this->{$this->getAssertBoolMethod($expectedResult)}(
            $courseViewsCount->decide()
        );
    }

    /**
     * ViewsProvider
     * @return Generator
     */
    public function viewsProvider()
    {
        yield [1, true];
        yield [5, true];
        yield [8, true];
        yield [10, false];
        yield [16, false];
        yield [45, false];
    }
}