<?php declare(strict_types=1);


namespace App\Tests\CourseAccess\Rules;

use App\CourseAccess\Rules\AdminRole;
use App\CourseAccess\Rules\SignedInRole;
use App\Tests\BaseTestCase;
use Generator;


class SignedInRuleTest extends BaseTestCase
{

    /**
     * TestDecide SignedIn Rule
     * @dataProvider roleProvider
     * @param  array  $role
     * @param  bool  $expectedResult
     */
    public function testDecide(array $role, bool $expectedResult): void
    {
        $adminRule = new SignedInRole($this->createSecurityMock($role));

        $this->{$this->getAssertBoolMethod($expectedResult)}(
            $adminRule->decide()
        );
    }

    /**
     * RoleProvider
     * @return Generator
     */
    public function roleProvider()
    {
        yield [['ROLE_USER'], true];
        yield [[], true];
    }
}