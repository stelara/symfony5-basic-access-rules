<?php declare(strict_types=1);


namespace App\Tests;

use App\Entity\User;
use App\Utill\DateTimeHelper;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Security\Core\Security;
use App\Repository\CourseViewRepository;

/**
 * Class BaseTestCase
 * @package App\Tests
 */
abstract class BaseTestCase extends TestCase
{

    /**
     * CreateSecurityMock
     * @param  array  $roles
     * @return Security
     */
    protected function createSecurityMock(array $roles = []): Security
    {
        $user = $this->createMock(User::class);
        $user->method('getRoles')->willReturn($roles);
        $security = $this->createMock(Security::class);
        $security->method('getUser')->willReturn($user);

        return $security;
    }

    /**
     * CreateCourseViewRepositoryViewsMock
     * @param  int  $views
     * @return CourseViewRepository
     */
    protected function createCourseViewRepositoryViewsMock(
        int $views
    ): CourseViewRepository {
        $courseViewRepository = $this->createMock(CourseViewRepository::class);

        $courseViewRepository
            ->method('getCourseViewsByUser')
            ->willReturn($views);

        return $courseViewRepository;
    }

    /**
     * CreateCourseViewRepositoryDateMock
     * @param  string  $date
     * @return CourseViewRepository
     */
    protected function createCourseViewRepositoryDateMock(
        string $date
    ): CourseViewRepository {
        $courseViewRepository = $this->createMock(CourseViewRepository::class);

        $courseViewRepository
            ->method('getLastCourseVisitByUser')
            ->willReturn($date);

        return $courseViewRepository;
    }

    /**
     * CreateDateTimeHelperPartialMock
     * @param $currentDate
     * @return DateTimeHelper
     */
    protected function createDateTimeHelperPartialMock(
        $currentDate
    ): DateTimeHelper {
        $dateTimeHelper = $this->createPartialMock(
            DateTimeHelper::class,
            ['getCurrentDate']
        );
        $dateTimeHelper
            ->method('getCurrentDate')
            ->willReturn($currentDate);

        return $dateTimeHelper;
    }

    /**
     * CreateParameterBagInterfaceMock
     * @param  array  $expectedValues
     * @param  string  $key
     * @return ParameterBagInterface
     */
    protected function createParameterBagInterfaceMock(
        array $expectedValues,
        string $key
    ): ParameterBagInterface {
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $parameterBag->method('get')
            ->willReturn($expectedValues[$key]);

        return $parameterBag;
    }

    /**
     * GetAssertBoolMethod
     * @param  bool  $expectedResult
     * @return string
     */
    protected function getAssertBoolMethod(bool $expectedResult): string
    {
        return $expectedResult ? 'assertTrue' : 'assertFalse';
    }
}