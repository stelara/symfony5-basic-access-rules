T&H Taste
=========

T&H Taste is a fake project to help you show what you're capable to the Taylor & Hart team.

It will force you to learn some new tools quickly and use them in a very basic context.

## Context

T&H Taste is a free online learning platform for cooks launched by Taylor & Hart. An admin publishes tutoring videos for cooking. The target market is PHP developers who have problems with cooking.

## Development

The following guidelines should be carefuly met.  
Even if you don't think they're best, now is not the time to show it.

* Use Symfony v5 Standard Edition 
* Logic should be done in services, not in models, not in controllers.
* Use unit testing at least for the most critical parts of the app
* Code must follow Symfony conventions. Tabulations, coding style, trailing spaces etc.
* You should provide fixtures to demo your code in a dev environment. Please use [Alice](https://github.com/nelmio/alice) to do so.
* Your README file should contain useful **basic** infos to get the project running. It should be written in English.
* Do not commit generated files that have no place in your project.
* Do not spend any time on HTML. Rather do the minimum with no CSS styling at all that shows that the feature is working.

## Features

* Courses are composed by a name and a YouTube video
  * We don't ask you to create an admin section to create them.
  * They should just be created via fixtures (dev environment)
* A user can register (email, password, username)
* A user can sign in (email, password)
* To be able to view a course page, a user has to be signed in and:
  * be an admin
  * OR have viewed less than 10 courses (different or not)
  * OR have waited a given time (say 1 day, but it should be a configuration option in `parameters.yml`) since its last view of a course - if he has already viewed more than 10 courses.
* In all other cases, the user only sees a page with the name of the course, but no video

## Expected delivery

* Start by creating a new project, using the standard [Symfony website skeleton](https://symfony.com/doc/current/setup.html)
* Commit incrementally in your repository.
* Send us an email with a link to your repository when you're done.

## License

This test specification is a property of Taylor & Hart.
You are not authorized to share it, and are expected to delete it with your own repository at the end of the application process.

## Thanks

Thank you for taking the time to do this little challenge!
